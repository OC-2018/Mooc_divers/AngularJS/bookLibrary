import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';

  constructor() {
    // Initialize Firebase
    const config = {
      apiKey: "AIzaSyBXGH0oQVJKAeH9_j0cYl5JgOueqRMTb4s",
      authDomain: "booklibrary-57f39.firebaseapp.com",
      databaseURL: "https://booklibrary-57f39.firebaseio.com",
      projectId: "booklibrary-57f39",
      storageBucket: "",
      messagingSenderId: "118558237404"
    };
    firebase.initializeApp(config);
  }
}
